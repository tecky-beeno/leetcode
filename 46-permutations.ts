let input = [1, 2, 3]

function permutate(input: number[]): number[][] {
  let output: number[][] = []
  permutateHelper([], input, output)
  return output
}

function permutateHelper(
  acc: number[],
  rest: number[],
  output: number[][],
): void {
  if (rest.length === 0) {
    output.push(acc)
    return
  }
  for (let i = 0; i < rest.length; i++) {
    let pickedValue = rest[i]
    let newRest = rest.slice()
    newRest.splice(i, 1)
    let newAcc: number[] = [...acc, pickedValue]
    permutateHelper(newAcc, newRest, output)
  }
}

let output = permutate(input)
console.log(output)
